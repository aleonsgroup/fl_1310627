
document.addEventListener("DOMContentLoaded", () => {


function createToken(url, login, password) {
    var result;
    var xhr = new XMLHttpRequest();
    var params =    'username=' + encodeURIComponent(login) +
                '&password=' + encodeURIComponent(password);
    xhr.open("POST", url+'/wp-json/jwt-auth/v1/token', false);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    try {
        xhr.send(params);
        result = JSON.parse(xhr.responseText)
    } catch (error) {
        result = JSON.parse('{"status":"error"}')
    }
    return result;
}

   if(localStorage.getItem('url_plugin_wp')!=undefined &&
    localStorage.getItem('login_plugin_wp')!=undefined &&
    localStorage.getItem('password_plugin_wp')!=undefined ){



        var createToken = createToken(localStorage.getItem('url_plugin_wp'), 
                                    localStorage.getItem('login_plugin_wp'),
                                    localStorage.getItem('password_plugin_wp')
        );
        if(createToken.token){
            localStorage.setItem('token_plugin_wp', createToken.token);
            initPosts();
            $('#setting').hide();
            $('#goMenu').hide();
            $('#posts').show();
            $('#last-img').show()
        }
        else if(createToken.data.token){
            localStorage.setItem('token_plugin_wp', createToken.data.token);
            initPosts();
            $('#setting').hide();
            $('#goMenu').hide();
            $('#posts').show();
            $('#last-img').show()
        }
        else{
            $('#error-msg').val('Вы указали неверные данные')
            $('#error-msg').show();
        }

   }
   else{
    $('#goMenu').show();
    $('#setting').show();
   }

   $('#saveSetting').on('click', function (e) {
    var url = $('#address').val();
    var login = $('#login').val();
    var password = $('#password').val();
    var result;
    try {
    var xhr = new XMLHttpRequest();
    var params =    'username=' + encodeURIComponent(login) +
                '&password=' + encodeURIComponent(password);
    xhr.open("POST", url+'/wp-json/jwt-auth/v1/token', false);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    try {
        xhr.send(params);
        result = JSON.parse(xhr.responseText)
    } catch (error) {
        result = JSON.parse('{"status":"error"}')
    }
    
        if(result.token){
            localStorage.setItem('token_plugin_wp', result.token);
            localStorage.setItem('url_plugin_wp', url);
            localStorage.setItem('login_plugin_wp', login);
            localStorage.setItem('password_plugin_wp', password);
            initPosts();
            $('#setting').hide();
            $('#goMenu').hide();
            $('#error-msg').hide();
            $('#posts').show();
            $('#last-img').show()
        }
        else if(result.data.token){
            localStorage.setItem('token_plugin_wp', result.data.token);
            localStorage.setItem('url_plugin_wp', url);
            localStorage.setItem('login_plugin_wp', login);
            localStorage.setItem('password_plugin_wp', password);
            initPosts();
            $('#setting').hide();
            $('#goMenu').hide();
            $('#posts').show();
            $('#last-img').show()
        }
        else{
            $('#error-msg').text('Вы указали неверные данные')
            $('#error-msg').show();
        }
    } catch (error) {
        $('#error-msg').text('Вы указали неверные данные')
        $('#error-msg').show();
    }

   });

   $('#posts').on('change', function (e) {
        localStorage.setItem('post_id_plugin_wp', $("option:selected", this).val());
        //$('#last-img').hide();
        try {
            storage.removeItem('last_update_post_plugin_wp');
        } catch (error) {
            
        }
    });

    $('#menu').on('click', function (e) {
        $(document.body).append(
            $('<div>').addClass('podlogka'),
            $('<div>').addClass('menu').append(
                $('<div>').append(
                    '<svg class="bi bi-x ml-1 mt-1" style="color: B0ACAC" width="2em" height="2em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M11.854 4.146a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708-.708l7-7a.5.5 0 0 1 .708 0z"/><path fill-rule="evenodd" d="M4.146 4.146a.5.5 0 0 0 0 .708l7 7a.5.5 0 0 0 .708-.708l-7-7a.5.5 0 0 0-.708 0z"/></svg>'
                ).css("cursor","pointer").on('click', function (e) {
                    $('.menu').remove();
                    $('.podlogka').remove();
                }),
                $('<button>').addClass('btn btn-outline-primary ml-3 mt-5').text('Настройки').on('click', function (e) {
                    $('#posts').hide();
                    $('#last-img').hide();
                    $('.menu').remove();
                    $('.podlogka').remove();
                    $('#setting').show();
                    $('#goMenu').show();
                })
            )
        )
    });
    $('#goMenu').on('click', function (e) {
        $('#setting').hide();
        $('#goMenu').hide();
        $('#posts').show();
        $('#last-img').show();
    });

});

function initPosts(){
    var posts = [];
        var postArr = getDraftPosts(localStorage.getItem('token_plugin_wp'));
        postArr.forEach(function(post) {
            var postObj = {};
            postObj.id=post.id;
            postObj.name = post.title.rendered;
            posts.push(postObj);
        });
        localStorage.setItem('posts_plugin_wp', JSON.stringify(posts));
        var postsSelect = $('#posts');
        var lastUpdatePost;
        try{
            lastUpdatePost = JSON.parse(localStorage.getItem('last_update_post_plugin_wp'))
        }
        catch{
            lastUpdatePost = null;
        }
        posts.forEach(function(post) {
            var option =$('<option>').text(post.name).val(post.id);
            if(lastUpdatePost!=null && lastUpdatePost.id == post.id){
                option.prop('selected', true);
                $('#last-img').attr('src', lastUpdatePost.urlImg);
                $('#last-img').show();
            }
            postsSelect.append(
                option
            );

    });
}



function getDraftPosts(token) {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", localStorage.getItem('url_plugin_wp')+'/wp-json/wp/v2/posts?status=draft', false);
    xhr.setRequestHeader('Authorization', 'Bearer '+token);
    xhr.send();
    result = JSON.parse(xhr.responseText)
    return result;
}



