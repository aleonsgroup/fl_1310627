chrome.contextMenus.create({
    "id": "add_in_id",
    "title": "Добавить в пост",
    "contexts": ["image"],
});


var qqq_i
var qqq_t

chrome.contextMenus.onClicked.addListener(function(info, tab) {
    function getHTMLOldPost(text) {
        var newText = text;

        //это можно убрать 
        newText =   text.replace('<h2>', '<!-- wp:heading --><h2>').replace('</h2>','</h2><!-- /wp:heading -->')
                        .replace('<figure class=\"wp-block-image\"><img src=','<!-- wp:image --><figure class=\"wp-block-image\"><img src=').replace('</a></figcaption></figure>','</a></figcaption></figure><!-- /wp:image -->')
        ////////
        
        return newText;
    }

    if( localStorage.getItem('token_plugin_wp') != undefined && 
        localStorage.getItem('post_id_plugin_wp') != undefined){

            var json = {};
            var xhr = new XMLHttpRequest();
            xhr.open("GET", localStorage.getItem('url_plugin_wp')+'/wp-json/wp/v2/posts/'+localStorage.getItem('post_id_plugin_wp'), false);
            xhr.setRequestHeader('Authorization', 'Bearer '+localStorage.getItem('token_plugin_wp'));
            xhr.send();
            post = JSON.parse(xhr.responseText)

            
            var site="";
            if(info.pageUrl.includes('https://pikabu.ru')){
                site = "pikabu";
            }
            else if(info.pageUrl.includes('https://www.reddit.com')){
                site = "reddit";
            }
            else if(info.pageUrl.includes('https://www.instagram.com')){
                site = "instagram";
            }
            else if(info.pageUrl.includes('https://twitter.com/')){
                site = "twitter";
            }

            if(site!=""){
                chrome.tabs.sendMessage(tab.id, {site: site, imgUrl: info.srcUrl}, function(response) {
                    var h2 ="";
                    var linkText = "";
                    var link = "";
                    if(response.h2){
                        h2=response.h2;
                    } 
                    if(response.linkText){
                        linkText=response.linkText;
                    }
                    if(response.link){
                        link = response.link;
                    }
                        json.content = getHTMLOldPost(post.content.rendered) +"\n<!-- wp:heading --><h2>"+h2+"</h2><!-- /wp:heading --><!-- wp:image --><figure class=\"wp-block-image\"><img src=\""+info.srcUrl+"\" alt=\"\"/><figcaption><a href=\""+link+"\">"+linkText+"</a></figcaption></figure><!-- /wp:image -->\n"
                        xhr.open("PUT", localStorage.getItem('url_plugin_wp')+'/wp-json/wp/v2/posts/'+localStorage.getItem('post_id_plugin_wp'), false);
                        xhr.setRequestHeader('Authorization', 'Bearer '+localStorage.getItem('token_plugin_wp'));
                        xhr.setRequestHeader('Content-Type', 'application/json');
                        xhr.send(JSON.stringify(json));
                        result = JSON.parse(xhr.responseText);
                        var lastUpdatePost = {};
                        lastUpdatePost.id=post.id;
                        lastUpdatePost.name=post.title.rendered;
                        lastUpdatePost.urlImg=info.srcUrl;
                        localStorage.setItem('last_update_post_plugin_wp', JSON.stringify(lastUpdatePost));

                    
                });
            }

            
        }
});



