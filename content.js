


chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {
        var h2 = "";
        var linkText = "";
        var link = "";


        switch (request.site) {
            case "pikabu":
                link = document.location.href;               
                if(document.querySelector('[src="'+request.imgUrl+'"]').closest('.story__main').getElementsByTagName('header').length>0){
                    h2=document.querySelector('[src="'+request.imgUrl+'"]').closest('.story__main').getElementsByTagName('header')[0].innerText
                    if(document.querySelector('[src="'+request.imgUrl+'"]').closest('.story__main').getElementsByTagName('header')[0].getElementsByTagName('a').length>0){
                        link = document.querySelector('[src="'+request.imgUrl+'"]').closest('.story__main').getElementsByTagName('header')[0].getElementsByTagName('a')[0].href;
                    }
                }
                linkText = document.querySelector('[src="'+request.imgUrl+'"]').closest('.story__main').getElementsByClassName('user__info-item')[0].innerText;
            break;

            case "reddit":
                var post = document.querySelector('[src="'+request.imgUrl+'"]').closest('div[data-click-id="background"]');

                if(post.getElementsByTagName('h3').length>0){
                    h2=post.getElementsByTagName('h3')[0].innerText;
                    link = post.getElementsByTagName('h3')[0].closest('a').href;
                }
                else if(post.getElementsByTagName('h1').length>0){
                    h2=post.getElementsByTagName('h1')[0].innerText;
                    link = document.location.href;               
                }
                Array.from(post.getElementsByTagName('a')).forEach((element, index, array) => {
                    if(array[index].innerText.includes('u/')){
                        linkText = array[index].innerText.replace('u/', '')
                        return h2;
                    }
                })               
            break;

            case "twitter":
                if(document.querySelector('[src="'+request.imgUrl+'"]').closest('article').querySelectorAll('[lang]').length>0){
                    h2=document.querySelector('[src="'+request.imgUrl+'"]').closest('article').querySelectorAll('[lang]')[0].innerText
                }
                if(document.querySelector('[src="'+request.imgUrl+'"]').closest('article').querySelector('time')){
                    link = document.querySelector('[src="'+request.imgUrl+'"]').closest('article').querySelector('time').closest('a').href;
                }
                else{
                    link = document.location.href;               
                }
                linkText=document.querySelector('[src="'+request.imgUrl+'"]').closest('article').getElementsByTagName('span')[0].innerText;
            break;


            // case "instagram":
            //     var header;
            //     if(document.querySelector('[src="'+request.imgUrl+'"]').parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.nodeName=="HTML"){
            //         header = document.querySelector('[src="'+request.imgUrl+'"]').parentElement.parentElement.parentElement.parentElement.parentElement.getElementsByTagName('header')[0];
            //     }
            //     else if(document.querySelector('[src="'+request.imgUrl+'"]').parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.nodeName=="HTML"){
            //         header = document.querySelector('[src="'+request.imgUrl+'"]').parentElement.parentElement.parentElement.parentElement.parentElement.getElementsByTagName('header')[0];
            //     }
            //     else if(document.querySelector('[src="'+request.imgUrl+'"]').parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.nodeName=="HTML"){
            //         header = document.querySelector('[src="'+request.imgUrl+'"]').parentElement.parentElement.parentElement.parentElement.parentElement.getElementsByTagName('header')[0];
            //     }
            //     else if(document.querySelector('[src="'+request.imgUrl+'"]').parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.nodeName=="DIV"){
            //         header = document.querySelector('[src="'+request.imgUrl+'"]').parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.getElementsByTagName('header')[0];
            //     }
            //     else if(document.querySelector('[src="'+request.imgUrl+'"]').parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.nodeName=="ARTICLE"){
            //         header = document.querySelector('[src="'+request.imgUrl+'"]').parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.getElementsByTagName('header')[0];
            //     }
            //     else if(document.querySelector('[src="'+request.imgUrl+'"]').parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.nodeName=="ARTICLE"){
            //         header = document.querySelector('[src="'+request.imgUrl+'"]').parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.getElementsByTagName('header')[0];
            //     }
            //     else if(document.querySelector('[src="'+request.imgUrl+'"]').parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.nodeName=="HTML"){
            //         header = document.querySelector('[src="'+request.imgUrl+'"]').parentElement.parentElement.parentElement.parentElement.parentElement.getElementsByTagName('header')[0];
            //     }

            //     if(header != undefined){
            //         linkText = header.getElementsByTagName('a')[0].innerText
            //         link = header.getElementsByTagName('a')[0].href
            //     }
            // break;

        }
        
        sendResponse({h2: h2, linkText: linkText, link: link});

        
    });